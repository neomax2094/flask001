#!/usr/bin/env python
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import app

db = app.db
ap = app.app

migrate = Migrate(ap, db)

manager = Manager(ap)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
