from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from source.resources.product import ProductResource

app = Flask(__name__)
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)
app.config.from_pyfile('settings/config.py')
api.add_resource(ProductResource, '/')

from source.models import models


if __name__ == '__main__':
    app.run(debug=True)