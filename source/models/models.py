from app import db


class ObjectBase(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(120), unique=True)
    description = db.Column(db.Text)


class AttributeBase(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(120), unique=True)
    object_base_id = db.Column(db.Integer, db.ForeignKey('object_base.id'))
