from flask import make_response, jsonify
from flask_restful import Resource

class ProductResource(Resource):
    @staticmethod
    def get():
        return make_response(jsonify({'hello': 'world'}), 200)